# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 15:41:32 2019

@author: DNP_Student_3
"""

#%% Load packages
import numpy as np
from numpy import linalg
import scipy 
import scipy.sparse
from scipy.stats import norm
from scipy.stats import chi2
import scipy.io as sio
import os
import random
import math
import time
from copy import copy
from numba import jit, prange, njit, guvectorize
import numba
from joblib import Parallel, delayed
import llvmlite.binding as llvm
import dask
#%% Point Process Functions (PNAS)
# Generate Spikes
def GenerateSpikes(thetac,mu,Cmap):
    Mc = len(thetac)
    [Ncells,a,T] = np.shape(Cmap)

    nex = np.zeros([T+Mc,Ncells])
    ld = np.zeros([T,Ncells])
    lHist = np.zeros([T,Ncells])

    for k in range(0,T):
        for cc in range(0,Ncells):
            idx = np.nonzero(Cmap[cc,:,k])[0]
            lgc = np.zeros([max(idx)+1])
            for ee in idx:
                lgc[ee] = np.dot(np.dot(Cmap[cc,ee,k],thetac.T),nex[k+Mc:k:-1,ee])
                lHist[k,cc] = sum(lgc)
                lc = mu + lHist[k,cc]
                ld[k,cc] = np.exp(lc)/(1+np.exp(lc))
                if random.uniform(0,1) <= ld[k,cc]:
                    nex[k+Mc,cc] = 1
                else:
                    nex[k+Mc,cc] = 0
    resp = nex[Mc:T+Mc,:]
    return resp

# FormHistMatrix
def FormHistMatrix(x,Whc):
    [T,Ncells] = np.shape(x) 
    Mhc = np.shape(Whc)[1]
    Lhc = np.sum(Whc)
    ne  = np.zeros([int(Lhc),Ncells])
    nex = np.vstack((ne,x))

    X = np.zeros([T,Mhc,Ncells])
    Lhself1 = np.zeros([Mhc,1])
    Lhself2 = np.zeros([Mhc,1])
    for j in range(0,Mhc):
        Lhself1[j] = np.sum(Whc[0,Mhc-j:Mhc])
        Lhself2[j] = np.sum(Whc[0,Mhc-j-1:Mhc])
    for k in range(0,T):
        for cc in range(0,Ncells):
            for j in range(1,Mhc+1):
                X[k,(Mhc-j+1)-1,cc] = np.sum(nex[k+int(Lhself1[j-1]):k+1+int(Lhself2[j-1])-1,cc])
    Xz = X[:]
    for cc in range(0,Ncells):
        Xz[:,:,cc] = X[:,:,cc] - np.dot(np.ones([T,1]),np.expand_dims(np.mean(X[:,:,cc],axis = 0),1).T)
    return Xz

# FormFull design
def FormFullDesign(Xcross,Xself,ct):
    [T,Mhc,Ncells] = np.shape(Xcross)
    [non,Mhcs] = np.shape(Xself)
    Mf = (Ncells-1)*Mhc+Mhcs+1
    cellC = list(np.arange(0,Ncells)) ; del cellC[ct]
    
    Xf = np.zeros([T,Mf])
    Xf[:,0] = np.ones([T])
    Xf[:,1:Mhcs+1] = Xself
    
    for cc in range(0,Ncells-1):
        Xf[:,cc*Mhc+Mhcs+1:(cc+1)*Mhc+Mhcs+1] = Xcross[:,:,cellC[cc]]
    VarXf = np.var(Xf,axis=0); VarXf[0] = 1
    VarXf = np.expand_dims(VarXf,axis=1).T
    Dfn = np.zeros(np.shape(Xf))
    for n in range(0,T):
        Dfn[n,:] = np.sqrt(VarXf)
    Xfn = Xf/Dfn
    return Xfn

#%SoftThreshold
def SoftThreshold(x,gamma):
    y = np.multiply(np.sign(x),np.maximum(np.absolute(x)-gamma,[0]))
    return y

#%L1PFF1
def L1PPF1(X,n,ff,gamma,LL,cw,W,w0,v0,U0):
    [N,M] = np.shape(X)
    K= math.ceil(N/W)
    what = np.zeros([K,M])
    wl = copy(w0)
    vk = copy(v0); Uk = copy(U0)
    navg = np.mean(n)
    gammaEff = gamma*np.sqrt(W*np.log(M)*navg*(1-navg)/(1-ff))
    Lipk = 0
    for k in range(0,K):
        Xk = X[(k)*W:(k+1)*W,:]
        nk = n[(k)*W:(k+1)*W]
        for l in range(0,LL):
            ykl = np.dot(Xk,wl)
            lambdakl = np.exp(ykl)/(1+np.exp(ykl))
            kappakl = np.zeros([len(list(lambdakl)),len(list(lambdakl))])
            for p in range(0,len(list(lambdakl))):
                kappakl[p,p] = lambdakl[p]*(1-lambdakl[p])
                if lambdakl.ndim == 2:
                    ekl = nk - np.squeeze(lambdakl, axis = 1)
                else: 
                    ekl = nk - lambdakl
            vkl = np.dot(ff,vk) +np.dot(Xk.T,(np.expand_dims(ekl,1)+np.dot(kappakl,ykl)))
            Ukl = np.dot(ff,Uk) +np.dot(Xk.T,np.dot(kappakl,Xk))
            Lipkl = ff*Lipk + np.sum(np.diagonal(np.dot(kappakl,(np.dot(Xk,Xk.T)))))
            al = (1-ff)/np.dot(cw,W)
            gl = vkl - np.dot(Ukl,wl)
            wl = SoftThreshold(wl+np.dot(al,gl), np.dot(gammaEff,al))
        what[k,:] = wl.T
        vk = copy(vkl); Uk = copy(Ukl); Lipk = copy(Lipkl);
    return what, vkl, Ukl

# CrossValidwin     
def CrossValidwin(Respx,gammacv,Whcv,Whscv,KK,ffcv,Wcv,ccv,LLcv):
    [Nb, Ncells] = np.shape(Respx)    
    LLcost = np.zeros([len(Whcv),len(gammacv),KK,Ncells])

    for ii in range(0,np.size(Whcv)):
        Whi = Whcv[ii] ; [non,Mc] = np.shape(Whi)
        Xc = FormHistMatrix(Respx,Whi)
        Whsi = Whscv[ii]; [non,Mhcs] = np.shape(Whsi)
        Mf = (Ncells-1)*Mc+Mhcs+1
        for ct in range(0,Ncells):
            N_indto = str(ct+1)
            print("Cross-validation on cell " + N_indto + "..." )
            robs = np.expand_dims(Respx[:,ct], axis=1)
            Xcs = FormHistMatrix(robs,Whsi)
            Xcs = np.squeeze(Xcs,axis=2)
            Xfn = FormFullDesign(Xc,Xcs,ct)
            navg = np.mean(robs)
            w0f = np.zeros([Mf,1]); w0f[0] = math.log(navg/(1-navg))
            v0f = np.zeros([Mf,1]); U0f = 1*np.identity(Mf)
            start = time.time()
            for kk in range(0,KK):
                print("CV on fold" + str(kk+1) + " ....")
                Indvalid = np.zeros([Wcv,int(Nb/(KK*Wcv))]) 
                for j in range(0,int(Nb/(KK*Wcv))): 
                    Indvalid[:,j] = range(KK*Wcv*(j)+(kk)*Wcv,KK*Wcv*(j)+kk*Wcv+Wcv)
                indvalid = np.reshape(Indvalid,Indvalid.size,1)
                indvalid = [int(x) for x in indvalid]
                nvalid = robs[indvalid]; Svalid = Xfn[indvalid,:]
                ntrain = copy(robs) ; ntrain = np.delete(ntrain,indvalid)
                Strain = copy(Xfn); Strain = np.delete(Strain,indvalid,axis =0)
                for jj in range(0,len(gammacv)):
                    gammaj = gammacv[jj]
                    print("CV @ gamma = " + str(gammaj) + " ..." )
                    [Wtrain,no,non] = L1PPF1(Strain,ntrain,(ffcv**KK),gammaj,LLcv,ccv,Wcv,w0f,v0f,U0f)
                    for k in range(0,len(nvalid)):
                        LLcost[ii,jj,kk,ct] = LLcost[ii,jj,kk,ct] + np.dot(nvalid[k],np.dot(Svalid[k,:],np.expand_dims(Wtrain[math.ceil(k/Wcv)-1,:],1))) - np.log(1+np.exp(np.dot(Svalid[k,:],np.expand_dims(Wtrain[math.ceil(k/Wcv)-1,:],1))))
            end = time.time()
            print("Elapsed time is " + str(end - start))
    LLavg = np.squeeze(np.mean(LLcost,axis=2))
    gammaOpt = np.zeros([Ncells])
    for ct in range(0,Ncells):
        LLct = LLavg[:,ct]
        indgamma = np.argmax(LLct)
        gammaOpt[ct] = gammacv[np.max(indgamma)]
    return gammaOpt

# L1PPF1ModDev
def L1PPF1ModDev(X,n,ff,gamma,LL,cw,W,w0,u0,v0,U0):
    [N,M] = np.shape(X)
    K = math.ceil(N/W)
    what = np.zeros([K,M])
    Devk = np.zeros([K,1])
    Bk = copy(Devk)
    wl = copy(w0)
    uk = copy(u0); vk = copy(v0); Uk = copy(U0)
    Pk = 0.001* np.identity(M)
    Neff = W/(1-ff)
    navg = np.mean(n)
    gammaEff = gamma*np.sqrt(Neff*np.log(M)*navg*(1-navg))
    for k in range(0,K):
        Xk = X[(k)*W:(k+1)*W,:]
        nk = n[(k)*W:(k+1)*W]    
        for l in range(0,LL):
            ykl = np.dot(Xk,wl)
            lambdakl = np.exp(ykl)/(1+np.exp(ykl))
            kappakl = np.zeros([len(list(lambdakl)),len(list(lambdakl))])
            for p in range(0,len(list(lambdakl))):
                kappakl[p,p] = lambdakl[p]*(1-lambdakl[p])
            ekl = nk - lambdakl
            ukl = np.dot(ff,uk) + np.dot(lambdakl.T,ykl) - np.sum(np.log(1+np.exp(ykl))) - np.dot((1/2),np.dot(ykl.T,np.dot(kappakl,ykl)))
            vkl = np.dot(ff,vk) +np.dot(Xk.T,(ekl+np.dot(kappakl,ykl)))
            Ukl = np.dot(ff,Uk) +np.dot(Xk.T,np.dot(kappakl,Xk))
            al = 1/(cw*Neff)
            gl = vkl - np.dot(Ukl,wl)
            wl = SoftThreshold(wl+np.dot(al,gl), np.dot(gammaEff,al))
        what[k,:] = wl.T
        Yk = np.dot(Pk,Xk.T)
        rd1 = np.dot(ff,np.identity(W))
        rd2 = np.linalg.solve(kappakl.T,rd1.T).T + np.dot(Xk,Yk)
        rd3 = Pk - np.dot(np.linalg.solve(rd2.T,Yk.T).T,Yk.T)
        [r,c] = np.shape(rd3)
        Pk[:,:] = [x / ff for x in rd3]
        Bk[k] = np.dot(gl.T,np.dot(Pk,gl))
        Devk[k] = 2*(ukl + np.dot(vkl.T,wl) - 0.5*np.dot(wl.T,np.dot(Ukl,wl))) + Bk[k]
        uk = copy(ukl); vk = copy(vkl); Uk = copy(Ukl);
    return what, Devk, ukl, vkl, Ukl

# NoncentChi2FiltSmooth    
def NoncentChi2FiltSmooth(Devk,Md,sz0,rho,NN,Nem):
    [K,Ncells,non] = np.shape(Devk)
    nukFilt = np.zeros([K,Ncells,Ncells])
    nukFiltU = np.zeros([K,Ncells,Ncells])
    nukFiltL = np.zeros([K,Ncells,Ncells])
    nukSmth= np.zeros([K,Ncells,Ncells])
    nukSmthU = np.zeros([K,Ncells,Ncells])
    nukSmthL = np.zeros([K,Ncells,Ncells])
    eta= norm.ppf(0.975)
    
    for ct in range(0,Ncells):
        cellC = list(range(0,Ncells)); del cellC[ct]
        for cf in cellC:
            Dnk = Devk[:,ct,cf]
            zk = np.zeros([K+1,1]); s2k = np.zeros([K+1,1])
            nuk = np.zeros([K,1]); nukU = np.zeros([K+1,1]); nukL = np.zeros([K+1,1])
            sz = copy(sz0)
            for ll in range(0,Nem):
                zk[0] = 0
                s2k[0] = 1
                for k in range(0,K):
                    zl = rho*zk[k]
                    for l in range(0,NN):
                        nul = np.exp(zl)
                        ul2 = nul*np.maximum(Dnk[k],[0])
                        fl = np.sqrt(ul2 + (Md-1)**2/4) - (Md-1)/2
                        gl = zl - rho*zk[k] + (rho**2*s2k[k]+sz)*(nul-fl)/2
                        gpl = 1 + (rho**2*s2k[k]+sz)*(nul-ul2/(2*(fl+(Md-1)/2)))/2
                        zl = zl - gl/gpl
                    zk[k+1] = zl
                    nuk[k] = np.exp(zk[k+1])
                    uzk = np.sqrt(nuk[k]*np.maximum(Dnk[k],[0]))
                    fuk = np.sqrt(uzk**2+ (Md-1)**2/4) - (Md-1)/2
                    s2k[k+1] = 1/(1/(rho**2*s2k[k]+sz) + nuk[k]/2 - (uzk**2- (Md-2)*fuk - fuk**2)/4)
                    nukU[k+1] = np.exp(zk[k+1] + eta*np.sqrt(s2k[k+1]))
                    nukL[k+1] = np.exp(zk[k+1] - eta*np.sqrt(s2k[k+1]))
                zkK = copy(zk); s2kK = copy(s2k)
                s2kplK = np.zeros([K,1])
                KK = list(range(0,K))
                for k in KK:
                    k = 499-k
                    sk = rho*s2k[k+1]/(rho**2*s2k[k+1]+sz)
                    zkK[k] = zk[k+1]+sk*(zkK[k+1]- rho*zk[k+1])
                    s2kK[k] = s2k[k+1]+ (sk**2)*(s2kK[k+1] - (rho**2*s2k[k+1]+sz))
                    s2kplK[k] = sk*s2kK[k+1]
                nukKU = copy(nukU); nukKL = copy(nukL);
                for k in range(0,K):
                    nukKU[k] = np.exp(zkK[k] + eta*np.sqrt(s2kK[k]))
                    nukKL[k] = np.exp(zkK[k] - eta*np.sqrt(s2kK[k]))
                
                alpha = -1; beta = 0
                Eterm = rho**2*zkK[0]**2 + (1+rho**2)*np.sum(zkK[1:K]**2) + np.power(zkK[K],[2]) + rho**2*s2kK[0] + (1+rho**2)*np.sum(s2kK[1:K]) + s2kK[K] - 2*rho*(np.sum(s2kplK) + np.sum(np.multiply(zkK[1:K+1],zkK[0:K])))
                szp = (Eterm/2+beta)/(K/2+alpha+1)
                sz = copy(szp)
            nukFilt[:,ct,cf] = np.squeeze(nuk,axis=1)
            nukFiltU[:,ct,cf] = np.squeeze(nukU[1:],axis=1)
            nukFiltL[:,ct,cf] = np.squeeze(nukL[1:], axis=1)
            nukSmth[:,ct,cf] = np.squeeze(np.exp(zkK[1:]),axis=1)
            nukSmthU[:,ct,ct] = np.squeeze(nukKU[1:], axis=1)
            nukSmthL[:,ct,ct] = np.squeeze(nukKL[1:], axis=1)
    return nukSmth, nukSmthU, nukSmthL, nukFilt, nukFiltU, nukFiltL
    
    
# FDRcontrolBY
#Devk = Devkt; nuk = nukSmth; wfk = wknft; 
def FDRcontrolBY(Devk,nuk,alpha,Md,wfk,Whc):
    [N,Ncells,non] = np.shape(Devk)
    Mhc = len(np.squeeze(Whc,axis=0))
    [no,Mf] = np.shape(wfk[0])
    Mhcs = Mf -(Ncells-1)*Mhc-1
    Nl = Ncells*(Ncells-1)
    pth = np.zeros([1,Nl])
    for i in range(1,Nl+1):
        pth[:,i-1]=alpha*(i)/(Nl*np.log(Nl))
    pth = np.squeeze(pth,axis=0)
    alpham = alpha*(Nl+1)/(2*Nl*np.log(Nl))
    AGC = np.zeros(np.shape(Devk)); AGCs = AGC
    Pv = chi2.sf(Devk,Md)
    for t in range(0,N):
        Pvt = Pv[t,:,:]
        Pvtsort= np.sort(Pvt,axis=None)
        Indsrt = np.argsort(Pvt.T,axis=None)
        Pvtsortx = Pvtsort[0:Nl]
        cnt = 0
        while Pvtsortx[cnt] < pth[cnt]:
            cnt=cnt+1
            if cnt == Nl:
                break
        siglind = Indsrt[0:cnt]
        cols = np.ceil(np.divide(siglind+1,Ncells))
        rows = (siglind+1) - np.dot((cols-1),Ncells)
        for ee in range(0,len(siglind)):
            AGC[t,int(rows[ee]-1),int(cols[ee]-1)] = 1 - alpham - scipy.stats.ncx2.cdf(scipy.stats.distributions.chi2.ppf((1-alpham),Md),Md,nuk[t,int(rows[ee]-1),int(cols[ee]-1)])
            whati = wfk[rows[ee]-1]
            CellC = list(range(1,Ncells+1)); del CellC[int(rows[ee]-1)]
            jji = np.where(CellC == cols[ee])
            jji = jji[0]
            if not jji:
                pass
            else:
                if np.sign(np.dot(whati[t,int((jji-1)*Mhc+Mhcs+1):int(jji*Mhc+Mhcs)+1],Whc.T)) !=0:
                    AGCs[t,int(rows[ee]-1),int(cols[ee]-1)] = np.dot(np.sign(np.dot(whati[t,int((jji-1)*Mhc+Mhcs+1):int(jji*Mhc+Mhcs)+1],Whc.T)),AGC[t,int(rows[ee]-1),int(cols[ee]-1)])
                else:
                   AGCs[t,int(rows[ee]-1),int(cols[ee]-1)] = AGC[t,int(rows[ee]-1),int(cols[ee]-1)]    
    return AGCs,AGC
    
#%% Francis Paper 
#@guvectorize([(float64[:], float64[:])],'(n,n)->(n,n)')
@njit
def MeanCol(x):
    res= np.zeros((1,len(x[0])))
    for i in range(0,len(x[0])):
        res[:,i] = np.mean(x[:,i])
    return res
# FormHistMatrix2
#Inputs:
# x = 2D-array varrying size dtype float64
# Whc = 1D-array 2, dtype float64
# LM = float64
# Outputs
# Xz = 3D-array varrying size dtype float 64
@njit
def FormHistMatrix2(x,Whc,Lm):
    [N,Ncells] = x.shape
    Np = N -Lm
    Mhc = len(Whc); Lhc = np.sum(Whc)
    X = np.zeros((int(Np),Mhc,Ncells))
    Lhself1 = np.zeros((Mhc,1)); Lhself2 = Lhself1.copy()
    for j in range(0,Mhc):
        Lhself1[j] = np.sum(Whc[Mhc-j:Mhc])
        Lhself2[j] = np.sum(Whc[Mhc-j-1:Mhc])
    
    for k in range(0,int(Np)):
        iStart = k + Lm - Lhc + 1
        iStop = k + Lm - Lhc 
        for j in range(1,Mhc+1):
            cind = np.arange(iStart+Lhself1[j-1][0], (iStop + Lhself2[j-1] + 1)[0])
            cInd = cind.astype(np.int32)
            if len(cInd) == 1:
                X[k,Mhc-j,:] = x[cInd-1,:]
            else:
                X[k,Mhc-j,:] = MeanCol(x[cInd-1,:])
                
    Xz = X.copy()
    for cc in range(0,Ncells):
        Xz[:,:,cc] = X[:,:,cc] - np.dot(np.ones((int(Np),1)),MeanCol(X[:,:,cc]))
    return Xz

## FormFullDesign2
#@njit
def FormFullDesign2(Xcross,Xself,ct):
    [Np,Mhc,Ncells,R] = Xcross.shape
    cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
    ## Full Model
#    Xfc = np.squeeze(Xself,axis=2)
    Xfcc = Xself.copy()
    Xfc = Xfcc[:,:,0]
    Xcrosss = Xcross[:,:,:,0]
    for cc in range(0,Ncells-1):
        Xfc = np.concatenate((Xfc,Xcrosss[:,:,cellC[cc]]),axis=1)
    Xf = Xfc.copy()
    ## Form effective covariate matrix
    Mf = np.shape(Xf)[1]
    Xeff = np.zeros([R*Np,Mf])
    if Xf.ndim ==3:
        for r in range(1,R+1):
            Xeff[(r-1)*Np:r*Np,:] = Xf[:,:,r-1]
    else:
        for r in range(1,R+1):
            Xeff[(r-1)*Np:r*Np,:] = Xf[:,:]
    Dnf = np.diag(np.sqrt(np.var(Xeff,axis=0)))
    Xneff = linalg.solve(Dnf.T,Xeff.T).T
    return Xneff, Dnf

## StaticEstimCV
# Inputs:
# Xn = 2D-array shape 1997,20 dtype float64
# robs = 2D-array shape 1997,1 dtype float64
# gamma = float64
# LL = int
# LR = int
# Outputs:
# wn = 2D-array shape 20,1 dtype float64
# sig2 = float64
@njit
def StaticEstimCV(Xn,robs,gamma,LL,LR):
    [N,M] = Xn.shape
    # Estimate variance using maximum-likelihood
    XXn = Xn.T.dot(Xn)
    Xdn = Xn.T.dot(robs)
    wML = linalg.solve(XXn,Xdn)
    sig2ML = (linalg.norm(robs-np.dot(Xn,wML))**2)/N
    # Set initials based on ML estimate
    sig2l = np.expand_dims(np.array((sig2ML)),0)
    wl = wML.copy()
    sig2l_old = np.expand_dims(np.array((10000.)),0)
    for ll in range(0,LR):
        XXl = XXn/np.sqrt(sig2l*N*np.log(M))
        Xdl = Xdn/np.sqrt(sig2l*N*np.log(M))
        # Step-size selection
        rhom = np.max(np.absolute(linalg.eigvals(XXl)))## Still needs checking 
        al = 0.9/rhom; alg  = al*gamma 
        wl_old = np.ones_like(wl)*np.inf
        for l in range(0,LL):
#            gl = Xdl - np.dot(XXl,wl)
#            x = wl + np.dot(al,gl)
#            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-np.dot(gamma,al),0))
            x = wl + al*(Xdl-XXl.dot(wl))
            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-alg,0))
            if linalg.norm(wl-wl_old) < 0.0001:
                break
            wl_old = wl.copy()
        #Compute variance
        sig2l = np.expand_dims(np.array(((linalg.norm(robs-np.dot(Xn,wl))**2)/N)),0)
        if linalg.norm(sig2l - sig2l_old) < 0.0001:
            break
        sig2l_old = sig2l.copy()            
    wn = wl.copy(); sig2 = sig2l.copy()
    return  wn, sig2

## StaticEstim
@njit
def StaticEstim(Xn,robs,gamma,LL,LR):
    [N,M] = Xn.shape
    # Estimate variance using maximum-likelihood
    XXn = Xn.T.dot(Xn)
    Xdn = Xn.T.dot(robs)
    wML = linalg.solve(XXn,Xdn)
    sig2ML = (linalg.norm(robs-np.dot(Xn,wML))**2)/N
    # Set initials based on ML estimate
    sig2l = np.expand_dims(np.array((sig2ML)),0)
    wl = wML.copy()
    sig2l_old = np.expand_dims(np.array((10000.)),0)
    for ll in range(0,LR):
        XXl = XXn/np.sqrt(sig2l*N*np.log(M))
        Xdl = Xdn/np.sqrt(sig2l*N*np.log(M))
        # Step-size selection
        rhom = np.max(np.absolute(linalg.eigvals(XXl)))## Still needs checking 
        al = 0.9/rhom; alg  = al*gamma 
        wl_old = np.ones_like(wl)*np.inf
        for l in range(0,LL):
#            gl = Xdl - np.dot(XXl,wl)
#            x = wl + np.dot(al,gl)
#            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-np.dot(gamma,al),0))
            XXlwl = XXl.dot(wl)
            x = wl + al*(Xdl-XXlwl)
            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-alg,0))
            if linalg.norm(wl-wl_old) < 0.0001:
                break
            wl_old = wl.copy()
        #Compute variance
        sig2l = np.expand_dims(np.array(((linalg.norm(robs-np.dot(Xn,wl))**2)/N)),0)
        if linalg.norm(sig2l - sig2l_old) < 0.0001:
            break
        sig2l_old = sig2l.copy()            
    wn = wl.copy(); sig2 = sig2l.copy()
    # Compute gradient at updated wl and sig2l
    gn = (Xdn - np.dot(XXn,wn))/sig2
    Hn = XXn/sig2
    # Compute Unbiased Deviance
    B = np.dot(gn.T,linalg.solve(Hn,gn)) ## Needs checking
    Dev = -N*np.log(sig2) + B
    return wl,sig2,Dev,B
    

## CrossValidModTrial
def CrossValidModTrial(Resp,gammacv,Whcv,Whscv,LLcv,LRcv):
    [N,R,Ncells] = Resp.shape
    Jcost = np.zeros((Ncells,len(gammacv)))
    Whi = Whcv.copy(); Mhc = len(Whi); Lhc = np.sum(Whi)
    #Prepare Self-History Conditions
    Whsi = Whscv.copy(); Mhcs = len(Whsi); Lhcs = np.sum(Whsi)
    Lm = np.max([Lhc,Lhcs])
    Np = N - Lm
    # Form Cross-History Covariates Matrices
    Xcz = np.zeros([int(Np),Mhc,Ncells,R])
    for r in range(0,R):
        Xcz[:,:,:,r] = FormHistMatrix2(Resp[:,r,:],Whi,Lm)
    # Form Observation arrays for all cells
    Robs = (Resp[int(Lm)::,:,:]).copy()
    # Zero mean observation vectors
    for cc in range(0,Ncells):
        Robs[:,:,cc] = Robs[:,:,cc] - np.dot(np.ones([int(Np),1]),np.expand_dims(np.mean(Robs[:,:,cc],axis=0),0))
    for ct in range(0,Ncells):
        N_indto = str(ct+1)
        print('Cross-validation on cell ' + N_indto + ' ...')
        cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
        # Form Self-History Covariates
        Xcsz = np.zeros([int(Np),Mhcs,R])
        for r in range(0,R):
            Xcsz[:,:,r] = np.squeeze(FormHistMatrix2(np.expand_dims(Resp[:,r,ct],1),Whsi,Lm),axis=2)
        # Form Effective Response Vector
        robsz = Robs[:,:,ct]
        # Form Full model design matrix
        Xfc = Xcsz.copy()
        for cc in range(0,Ncells-1):
            Xfc = np.concatenate((Xfc,Xcz[:,:,cellC[cc],:]),axis=1)
        Xf = Xfc.copy()
        # Prepare for Cross-Validation
        sig2test = np.zeros([R,len(gammacv)])
        # Training stage on all but one repititions
        for r in range(0,R):
            rc = np.arange(0,R); rc = np.delete(rc,r)
            print('CV @ repetition # ' + str(r+1) + ' ...')
            #Form Testing Data
            Xtest = Xf[:,:,r]
            rtest = robsz[:,r]; rntest = rtest/np.sqrt(np.var(rtest))
            ## Form Training Data
            [Xntrain,Dntrain] = FormFullDesign2(Xcz[:,:,:,rc],Xcsz[:,:,rc],ct)
            rtrain = robsz[:,rc]
            rntrain = rtrain/np.sqrt(np.var(rtrain,axis=0))
            SigTests = np.zeros([1,len(gammacv)])
            for jj in range(0,len(gammacv)):
                gammaj = gammacv[jj]
                [wntrain,non] = StaticEstimCV(Xntrain,rntrain,gammaj,LLcv,LRcv)
                wtrain = linalg.solve(Dntrain,wntrain)
                ## Test stage: Compute variance for give repetition
                SigTests[:,jj] = (linalg.norm(np.expand_dims(rntest,1)-np.dot(Xtest,wtrain))**2)/Np
            sig2test[r,:] = SigTests.copy()
        Jcost[ct,:] = np.mean(sig2test,axis=0)
    gammaOpt = np.zeros([Ncells,1])
    for ct in range(0,Ncells):
        LLct = Jcost[ct,:]
        indgamma = np.where(LLct == np.min(np.min(LLct)))[0]
        if len(gammacv) > 1:
            indgamma = np.max(indgamma)
        gammaOpt[ct] = gammacv[int(indgamma)]
    return gammaOpt, Jcost

## LF_estimGC
def LF_EstimGC(cellC,Mhc,Mhcs,gamma,LL,LR,Xneff,reff,Np,Ntrials,Devf):
    Br = np.zeros(np.shape(cellC))
    Devd = np.zeros([len(cellC)])
    rhatr_tmp = {}
    sig2rtotal_tmp = np.zeros([len(cellC)])
    for iC in range(0,len(cellC)):
        cf = cellC[iC]
        # Reduced Model
        Xnefr = copy(Xneff)
        cc = np.where(cellC == cf)[0]
        Cdel = np.arange(cc*Mhc+Mhc,(cc+1)*Mhc+Mhcs)
        Xnefr = np.delete(Xnefr,Cdel,1)
        #Estimation Setting
        [wnr,sig2r,Devr,Br[iC]] = StaticEstim(Xnefr,reff,gamma,LL,LR)
        #Save estimated parameters
        sig2rtotal_tmp[iC] = copy(sig2r)
        # Reconstructed observation vector reduced model
        rhatr = np.zeros([int(Np),Ntrials])
        for r in range(1,Ntrials+1):
            Xnr = copy(Xnefr[int((r-1)*Np):int(r*Np),:])
            rhatr[:,r-1] = np.dot(Xnr,wnr)
        rhatr_tmp[iC] = copy(rhatr)
        #Compute difference of Deviance Staistic
        Devd[iC] = Devf - Devr
    return Devd,rhatr_tmp,sig2rtotal_tmp,Br
        
## FDRcontrolBH
def FDRcontrolBH(Devtotal,DkSmth,alpha,Md,wftotal,Whc,Mlow):
    [Ncells,N] = np.shape(Devtotal)
    Mhc = len(Whc)
    [Mf] = np.shape(wftotal[0]); Mhcs = Mf - (Ncells-1)*Mhc
    Nl = Ncells*(Ncells-1) # Number of links
    pth = np.zeros([1,Nl])
    for i in range(1,Nl+1):
        pth[:,i-1]=alpha*(i)/(Nl*np.log(Nl))
    pth = np.squeeze(pth,axis=0)
    alpham = alpha*(Nl+1)/(2*Nl*np.log(Nl)) # Mean FDR by BH rule
    IGC = np.zeros(np.shape(Devtotal)); IGClow = copy(IGC); IGChigh = copy(IGC)
    # Compute p-values
    Pv = chi2.sf(Devtotal,Md)
    for t in range(0,N):
        Pvt = copy(Pv)
        Pvtsort= np.sort(Pvt,axis=None)
        Indsrt = np.argsort(Pvt.T,axis=None)
        Pvtsortx = Pvtsort[0:Nl]
        cnt = 0
        while Pvtsortx[cnt] < pth[cnt]:
            cnt=cnt+1
            if cnt == Nl:
                break
        siglind = Indsrt[0:cnt]
        cols = np.ceil(np.divide(siglind+1,Ncells))
        rows = (siglind+1) - np.dot((cols-1),Ncells)
        for ee in range(0,len(siglind)):
            IGC[int(rows[ee]-1),int(cols[ee]-1)] = 1 - alpham - scipy.stats.ncx2.cdf(scipy.stats.distributions.chi2.ppf((1-alpham),Md),Md,DkSmth[int(rows[ee]-1),int(cols[ee]-1)])
            whati = wftotal[rows[ee]-1]
            CellC = np.arange(0,Ncells); CellC = np.delete(CellC,int(rows[ee]-1))
            jji = np.where(CellC == cols[ee]-1)[0]
           
            wseff = np.multiply(whati[int(jji*Mhc+Mhc):int(jji*Mhc+Mhcs+2)],Whc.T)
            wslow = copy(wseff[0:Mlow])
            wshigh = copy(wseff[Mlow::])
            if np.sign(np.sum(wslow)) != 0:
                IGClow[int(rows[ee]-1),int(cols[ee]-1)] = np.sign(np.sum(wslow))*IGC[int(rows[ee]-1),int(cols[ee]-1)]
            else:
                IGClow[int(rows[ee]-1),int(cols[ee]-1)] = IGC[int(rows[ee]-1),int(cols[ee]-1)]
            if np.sign(np.sum(wshigh)) !=0:
                IGChigh[int(rows[ee]-1),int(cols[ee]-1)] = np.sign(np.sum(wshigh))*IGC[int(rows[ee]-1),int(cols[ee]-1)]
            else:
                IGChigh[int(rows[ee]-1),int(cols[ee]-1)] = IGC[int(rows[ee]-1),int(cols[ee]-1)]
                
    return IGClow, IGChigh
                

#computeGrangerCausality
def computeGrangerCausality(DFF,Ncells, cellids, gammacv,alpha, Md, WH, Mhc, Mhcs, LL, LR,LLcv, LRcv, Mlow):
    Devtotal = np.zeros([Ncells,Ncells])
    DevtotalC = {}
    DLLtotal = copy(Devtotal)
    wftotal = {}
    robstotal = {}
    sig2ftotal = np.zeros([Ncells,1])
    sig2rtotal = np.zeros([Ncells,Ncells])
    sig2rtotalC = {}
    Bftotal = np.zeros([Ncells,1])
    Brtotal = np.zeros([Ncells,Ncells])
    rhatftotal = {}
    rhatrtotal = {}
    rhatrtotalC = {}
    gammaOpttotal = np.zeros([Ncells,1])
    BrtotalC = {}
    
    #Select Cells
    DFF = DFF[:,:,cellids-1]
    [Nframes,Ntrials,Ncells] = np.shape(DFF)
    # Cross-History Kernel
    Whc = np.array([1,WH*np.ones(Mhc-1)])
    Lhc = np.sum(Whc)
    Whcv = {}; Whcv[0] = Whc
    #Self-History Kernel
    Whcs = np.array([1,WH*np.ones(Mhc-1)])
    Whscv= {}; Whscv[0] = Whcs
    Lhcs = np.sum(Whcs)
    Mf = (Ncells-1)*Mhc+Mhcs
    Mr = (Ncells-2)*Mhc+Mhcs
    
    # Cross-Validation
    [gammaOpt,Jcost] = CrossValidModTrial(DFF,gammacv,Whc,Whcs,LLcv,LRcv)
    gammaOpttotal[:,0] = np.squeeze(gammaOpt,axis=1)
    
    # Form Cross-History Covariate Arrays
    Lm = np.max([Lhc,Lhcs])
    Np = Nframes - Lm
    Xcz = np.zeros([int(Np),Mhc,Ncells,Ntrials])
    for r in range(0,Ntrials):
        Xcz[:,:,:,r] = FormHistMatrix2(DFF[:,r,:],Whc,Lm)
    # Form Observation arrays for all cells
    Robs = copy(DFF[int(Lm)::,:,:])
    # Zero-mean the observation vectors
    for cc in range(0,Ncells):
        Robs[:,:,cc] = Robs[:,:,cc] - np.dot(np.ones([int(Np),1]),np.expand_dims(np.mean(Robs[:,:,cc],axis=0),0))
    for ct in range(0,Ncells):
        print('Estimating G-Causality to cell ' + str(ct+1) + ' ...')
        cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
        gamma = gammaOpt[ct]
        
        # Form Self-history Covariate matrix
        Xcsz = np.zeros([int(Np),Mhc,Ntrials])
        for r in range(0,Ntrials):
            Xcsz[:,:,r] = np.squeeze(FormHistMatrix2(np.expand_dims(DFF[:,r,ct],1),Whcs,Lm),axis=2)
        # Form Effective Response Vector
        robsz = Robs[:,:,ct]
        reff = robsz.flatten('F'); Neff = len(reff)
        
        ## Full Model
        # Form Standardize Full Model Covariate Matrix
        [Xneff,Dnf] = FormFullDesign2(Xcz,Xcsz,ct)
        # Filtering/Estimation Setting
        [wnf,sig2f,Devf,Bf] = StaticEstim(Xneff,reff,gamma,LL,LR) 
        # Save Tuning AR Coeffs full model for each cell
        wftotal[ct] = copy(wnf)
        sig2ftotal[ct] = copy(sig2f)
        robstotal[ct] = copy(robsz)
        #Reconstructed observation vector full model
        rhatf = np.zeros([int(Np),Ntrials])
        for r in range(1,Ntrials+1):
            Xnf = Xneff[int((r-1)*Np):int(r*Np),:]
            rhatf[:,r-1] = np.dot(Xnf,wnf)
        rhatftotal[ct] = copy(rhatf)
        Bftotal[ct] = copy(Bf)
        [Devd,rhatr_tmp,sig2rtotal_tmp,Br] = LF_EstimGC(cellC,Mhc,Mhcs,gamma,LL,LR,Xneff,reff,Np,Ntrials,Devf)
        sig2rtotalC[ct] = copy(sig2rtotal_tmp)
        BrtotalC[ct] = copy(Br)
        DevtotalC[ct] = copy(Devd)
        rhatrtotalC[ct] = copy(rhatr_tmp)
        # Insert results into output variables
    for ct in range(0,Ncells):
        cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
        sig2rtotal[ct,cellC] = sig2rtotalC[ct]
        Brtotal[ct,cellC] = BrtotalC[ct]
        Devtotal[ct,cellC] = DevtotalC[ct]
        for iC in range(0,len(cellC)):
            if ct == cellC[iC]:
                pass
            else:
                rhatrtotal[ct,cellC[iC]] = rhatrtotalC[ct]
    # Statistical Test: J-statistics
    DkSmth = Devtotal - Md
    [GCJL,GCJH] = FDRcontrolBH(Devtotal,DkSmth,alpha,Md,wftotal,Whc,Mlow)
    return Devtotal, GCJL, GCJH, wftotal, sig2ftotal, sig2rtotal, robstotal, rhatftotal, rhatrtotal, Bftotal, Brtotal, cellids, gammaOpttotal, Whc, Whcv, Whcs, gammaOpt, Jcost
     
## Main Function   
def GrangerCausality(DFF,MaxCells):
#Parameter Estimation Settings
    LL = 1000
    LR = 10
    LLcv = 100
    LRcv = copy(LR)
    # Cross-history covariance settings
    WH = 2 # Window Length
    Mhc = 2 # Samples per cell
    # Self history covariance settings
    Mhcs = 2 # Samples per cell
    # Check for diff gamma for each cell
    gammacv = np.arange(0,3.2,0.2)
    # Satistical Test: J-statistics based on FDR
    Md = copy(Mhc)
    alpha = 0.5 # sig. level
    # Determine which segments of analysis window are used to determine inhibitory versus excitatory links
    Mlow = 2 # Limit length of window
    
# Gather data
    #Select subset with highest response var
    varR = np.zeros([np.size(DFF,2),1])
    if MaxCells < np.size(DFF,2):
        RR = np.size(DFF,1)
        for c in range(0,np.size(DFF,2)):
            varR[c] = np.mean(np.var(DFF[:,:,c]))
        varS = np.divide(np.dot(varR,RR),np.sum(RR))
        indm = np.argsort(varS[::-1])
        cellids = np.sort(indm[0:MaxCells])
    else:
        cellids = np.arange(1,MaxCells+1)
    Ncells = np.minimum(np.size(DFF,2),MaxCells)
# GCAnalysis          
    [Devtotal,GCJL,GCJH,wftotal,sig2ftotal,sig2rtotal,robstotal,rhatftotal,
    rhatrtotal,Bftotal,Brtotal,cellids,gammaOpttotal,Whc,Whcv,Whcs,gammaOpt,Jcost]  = computeGrangerCausality(DFF,
    Ncells, cellids, gammacv,alpha, Md, WH, Mhc, Mhcs,
    LL, LR,LLcv, LRcv, Mlow)

    Output = {'Parameters':{'MaxCells': MaxCells,'Whc':Whc,'Whcs':Whcs,'LL':LL,'LR':LR,'WH':WH,'Mhc':Mhc,'Mhcs':Mhcs,'gammacv':gammacv,'Md':Md,'alpha':alpha,'Mlow':Mlow},
              'Cross-validation':{'gammaOpt':gammaOpt,'Jcost':Jcost,'Whcv':Whcv,'LLcv':LLcv,'LRcv':LRcv},
              'GCAnalysis':{'Devtotal':Devtotal,'GCJL':GCJL,'GCJH':GCJH,'wftotal':wftotal,'sig2ftotal':sig2ftotal,'sig2rtotal':sig2rtotal,'robstotal':robstotal,'rhatftotal':rhatftotal,'rhatrtotal':rhatrtotal,'Bftotal':Bftotal,'Brtotal':Brtotal,'cellids':cellids,'gammaOpttotal':gammaOpttotal}}
    return Output    
   
#%%
TestData = sio.loadmat('TestData50')
Data  = TestData['ans']
#%% Analysis
DFF = np.copy(Data); MaxCells = 50
Output = GrangerCausality(DFF,MaxCells)




































    
    
    
    