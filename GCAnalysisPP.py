# -*- coding: utf-8 -*-
"""
Created on Mon Jan 14 14:34:45 2019

@author: DNP_Student_3
"""
#%% Matlab Times
#N = 8, 16, 32


#%% Load packages
import numpy as np
import scipy 
import scipy.sparse
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import random
import GCFuncs as GC
import math
import time
import scipy.io as sio
cwd = "C:\\Users\\DNP_Student_3"
os.chdir("C:\\Users\\DNP_Student_3\\Downloads")
from Corrfunc import corr2_coeff
os.chdir('C:\\Users\\DNP_Student_3\\Documents\\repos\\fishualizer_public')
from utilities import Zecording  # this is class built specifically to load zebrafish recordings
os.chdir(cwd)

#%% Spike generation
Tt = 120 # Total duration (sec)
delta = 0.001 # Time bin (sec)
T = Tt/delta # number of time bins

Ncells = 8;
Cmap = np.zeros([Ncells,Ncells,int(T)])
ExcInhID = np.expand_dims([1, -1, 1, -1, 1, -1, 1], axis =1)

for t in range(0,int(T)):
    Cmap[:,:,t] = (-1)*np.identity(Ncells)
    Cmap[1:,0,t] = np.stack(ExcInhID*np.ones([Ncells-1,1])*(1 - np.minimum(np.maximum((t+1)-T/3,0)*(1/(T/3)),1)), axis=1)
    CC5 = list(np.arange(0,Ncells)) ; del CC5[4];
    Cmap[CC5,4,t] = np.stack(ExcInhID*np.ones([Ncells-1,1])*(np.minimum(np.maximum((t+1)-T/3,0)*(1/(T/3)),1)), axis=1)
    Cmap[6,2,t] = 1; Cmap[5,7,t] = -1; Cmap[1,5,t] = 1;
#Cmap = sio.loadmat('Cmap')
#Cmap = Cmap['Cmap']

mspkmax = 0.08
muMax = np.log(mspkmax/(1-mspkmax))
mspkmin = 0.06
muMin = np.log(mspkmin/(1-mspkmin))
mspkavg = (mspkmax+mspkmin)/2
muBase = (muMax + muMin)/2

thetac = np.ones([10,1]) * [1,0,0,2,0,0,0,0,0,1]
thetac = np.transpose(thetac)
thetac = thetac.reshape(100,1)
Mc = len(thetac)
thetac = thetac/np.linalg.norm(thetac)
        
resp = GC.GenerateSpikes(thetac,muBase,Cmap)
#resp = sio.loadmat('resp')
#resp = resp['resp']

#%% Filtering and Deviance Computation
LL = 1
cw = 1
ff = 0.998
W = 20
Nw = int(T/W)
Devkt = np.zeros([Nw,Ncells,Ncells])
wknft = {}

WH = 10
Mhc = 10
Whc = WH*np.ones([1,Mhc])

Xcz = GC.FormHistMatrix(resp,Whc)        

Mhcs = 10
Whcs = WH*np.ones([1,Mhc])
Mf = (Ncells-1)*Mhc+Mhcs+1
Mr = (Ncells-2)*Mhc+Mhcs+1
Md = Mf - Mr

KK=2
Gammacv = np.arange(0.25,0.7,0.05)
Whcv = {}; Whcv[0] = Whc
Whscv = {}; Whscv[0] = Whcs

gammaOpt = GC.CrossValidwin(resp,Gammacv,Whcv,Whscv,KK,ff,W,cw,LL)
start = time.time()
for ct in range(0,Ncells):
    N_indto = str(ct+1)
    robs = resp[:,ct]
    robs = np.expand_dims(robs,1)
    cellC = list(range(0,Ncells)); del cellC[ct]
    cellC = np.array(cellC)
    Xcsz = GC.FormHistMatrix(robs,Whcs)
    Xcsz = np.squeeze(Xcsz,axis=2)
    Xfn = GC.FormFullDesign(Xcz,Xcsz,ct)
    navg = np.mean(robs)
    w0f = np.zeros([Mf,1]); w0f[0] = math.log(navg/(1-navg))
    u0f = 0; v0f = np.zeros([Mf,1]); U0f = 1*np.identity(Mf)
    gammact = gammaOpt[ct]
    gammaEffct = gammact*np.sqrt(W*np.log(Mf)*navg*(1-navg)/(1-ff))
    [wknf,Devkf,ukf,vkf,Ukf] = GC.L1PPF1ModDev(Xfn,robs,ff,gammact,LL,cw,W,w0f,u0f,v0f,U0f)
    wknft[ct] = wknf
    for cf in cellC:
        N_indform = str(cf+1)
        print("Estimating Causality from cell " + N_indform + " to cell " + N_indto + " ....")
        Xrn = Xfn
        cc = np.where(cellC == cf)
        cc = int(cc[0])
        Xrn = np.delete(Xrn,np.s_[cc*Mhc+Mhcs+1:(cc+1)*Mhc+Mhcs+1],axis=1)
        w0r = np.zeros([Mr,1]); w0r[0] = w0f[0]
        u0r = 0; v0r = np.zeros([Mr,1]); U0r = 1*np.identity(Mr)
        [wknr,Devkr,ukr,vkr,Ukr] = GC.L1PPF1ModDev(Xrn,robs,ff,gammact,LL,cw,W,w0r,u0r,v0r,U0r)
        Devkd = np.dot((1+ff),(Devkf-Devkr))
        Devkt[:,ct,cf] = np.squeeze(Devkd, axis = 1)
end = time.time()
print("Elapsed time is " + str(end - start))

start = time.time()
sz0 = 5*10**(-6)
rho = 1
NN =20
Nem = 1
[nukSmth,nukSmthU,nukSmthL,nukFilt,nukFiltU,nukFiltL] = GC.NoncentChi2FiltSmooth(Devkt,Md,sz0,rho,NN,Nem)

alpha = 0.1
[AGCs,AGC] = GC.FDRcontrolBY(Devkt,nukSmth,alpha,Md,wknft,Whc)

#%%Plots
# Spike trains
Lwin = 1000
Twin = np.divide([Tt/3,Tt/2,Tt],delta)
Ntw = len(Twin)

hfB = plt.figure()
for cc in range(1,Ncells+1):
    for tt in range(1,Ntw+1):
        plt.subplot(Ncells,Ntw,(cc-1)*Ntw+tt), plt.stem(resp[int(Twin[tt-1]-Lwin):int(Twin[tt-1]),cc-1])
        plt.axis('tight')
        if tt ==1:
            plt.ylabel('Cell '+ str(cc))
plt.show()            


# Estimates of Non-cenrality

ct = [1,3,5,1]
cf = [4,0,7,7]
nct = len(ct)
cols = ['b','r','g','m']
hfC = plt.figure()
for i in range(0,len(ct)):
    plt.plot(np.dot(range(0,Nw),np.dot(W,delta)),nukSmth[:,ct[i],cf[i]],cols[i],linewidth=2)
    plt.plot(np.dot(range(0,Nw),np.dot(W,delta)),nukSmthU[:,ct[i],cf[i]],cols[i])
    plt.plot(np.dot(range(0,Nw),np.dot(W,delta)),nukSmthL[:,ct[i],cf[i]],cols[i])
    plt.plot(np.dot(range(0,Nw),np.dot(W,delta)),Devkt[:,ct[i],cf[i]]-Md,'k')
plt.axis('tight')
plt.ylim((-Md,100))
plt.show()

# J-staistics
hfD = plt.figure()
for i in range(1,nct+1):
    plt.subplot(nct,1,i), plt.plot(np.dot(range(0,Nw),np.dot(W,delta)),AGC[:,ct[i-1],cf[i-1]],linewidth=2)
    plt.ylim((0,1))
    
# Estimated GC Maps Matrix
Tsnaps = list([20,30,40,50,60,80,90,100,120])
Nsnp = len(Tsnaps)

hfE = plt.figure()
cmax = +1; cmin = -cmax
for i in range(1,Nsnp+1):
    Cmapi = Cmap[:,:,int(Tsnaps[i-1]/delta)-1]
    Cmapi = Cmapi - np.diag(np.diagonal(Cmapi))
    nsub = 'ax' +str(i)
    plt.subplot(2,Nsnp,i), plt.imshow(Cmapi)
    
cmax = np.max(np.max(np.max(np.absolute(AGCs)))); cmin = -cmax
for k in range(1,Nsnp+1):
    Phik = AGCs[int(Tsnaps[k-1]/np.dot(delta,W))-1,:,:]
    plt.subplot(2,Nsnp,Nsnp+k), plt.imshow(Phik)
plt.show()































