import numpy as np
from numpy import linalg
from copy import copy
from numba import jit, prange, guvectorize
import time


## StaticEstimCV
# called by CrossValidModTrial(Resp,gammacv,Whcv,Whscv,LLcv,LRcv)
# which is called like this [gammaOpt,Jcost] = CrossValidModTrial(DFF,gammacv,Whcv,Whscv,LLcv,LRcv)
# [Xntrain,Dntrain] = FormFullDesign2(Xcz[:,:,:,rc],Xcsz[:,:,rc],ct)
# Calling [wntrain,non] = StaticEstimCV(Xntrain,rntrain,gammaj,LLcv,LRcv)
# Inputs:
# Xn = 2D-array shape 1997,20 dtype float64
# robs = 2D-array shape 1997,1 dtype float64
# gamma = float64
# LL = int
# LR = int
# Outputs:
# wn = 2D-array shape 20,1 dtype float64
# sig2 = float64
def StaticEstimCV(Xn, robs, gamma, LL, LR):
    """

    Parameters
    ----------
    Xn: 2D-array shape 1997,20 dtype float64
    robs: 2D-array shape 1997,1 dtype float64
    gamma: float64
    LL: int
    LR: int

    Returns
    -------
    wn: 2D-array shape 20,1 dtype float64
    sig2: float64
    """
    [N, M] = np.shape(Xn)
    # Estimate variance using maximum-likelihood
    XXn = np.dot(Xn.T, Xn)
    Xdn = np.dot(Xn.T, robs)
    wML = linalg.solve(XXn, Xdn)
    sig2ML = (linalg.norm(robs - np.dot(Xn, wML)) ** 2) / N
    # Set initials based on ML estimate
    sig2l = copy(sig2ML)
    wl = copy(wML)
    sig2l_old = np.inf
    for ll in range(0, LR):
        XXl = XXn / np.sqrt(sig2l * N * np.log(M))
        Xdl = Xdn / np.sqrt(sig2l * N * np.log(M))
        # Step-size selection
        rhom = np.max(np.absolute(linalg.eigvals(XXl)))  ## Still needs checking
        al = 0.9 / rhom;
        alg = al * gamma
        wl_old = np.inf

        for l in range(0, LL):
            #            gl = Xdl - np.dot(XXl,wl)
            #            x = wl + np.dot(al,gl)
            #            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-np.dot(gamma,al),0))
            x = wl + np.dot(al, (Xdl - np.dot(XXl, wl)))
            wl = np.multiply(np.sign(x), np.maximum(np.absolute(x) - alg, 0))
            if linalg.norm(wl - wl_old) < 0.0001:
                break
            wl_old = copy(wl)
        # Compute variance
        sig2l = (linalg.norm(robs - np.dot(Xn, wl)) ** 2) / N
        if linalg.norm(sig2l - sig2l_old) < 0.0001:
            break
        sig2l_old = copy(sig2l)
    wn = copy(wl);
    sig2 = copy(sig2l)
    return wn, sig2


@jit(nopython=True)
def numba_static_estim_cv(Xn, robs, gamma, LL, LR):
    """

    Parameters
    ----------
    Xn: np.ndarray
     2D-array shape 1997,20 dtype float64
    robs: np.ndarray
        2D-array shape 1997,1 dtype float64
    gamma: float64
    LL: int
    LR: int

    Returns
    -------
    wn: 2D-array shape 20,1 dtype float64
    sig2: float64
    """
    [N, M] = Xn.shape
    # Estimate variance using maximum-likelihood
    Xnt = Xn.transpose()
    XXn = Xnt.dot(Xn)
    Xdn = Xnt.dot(robs)
    wML = linalg.solve(XXn, Xdn)
    sig2ML = (linalg.norm(robs - Xn.dot(wML)) ** 2) / N
    # Set initials based on ML estimate
    sig2l = sig2ML  # copy(sig2ML)
    wl = wML.copy()
    sig2l_old = np.inf
    for ll in range(0, LR):
        XXl = XXn / np.sqrt(sig2l * N * np.log(M))
        Xdl = Xdn / np.sqrt(sig2l * N * np.log(M))
        # Step-size selection
        rhom = np.max(np.absolute(linalg.eigvals(XXl)))  ## Still needs checking
        al = 0.9 / rhom
        alg = al * gamma
        wl_old = np.ones_like(wl) * np.inf

        for l in range(0, LL):
            #            gl = Xdl - np.dot(XXl,wl)
            #            x = wl + np.dot(al,gl)
            #            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-np.dot(gamma,al),0))
            x = wl + al * (Xdl - XXl.dot(wl))
            wl = np.multiply(np.sign(x), np.maximum(np.absolute(x) - alg, 0))
            if linalg.norm(wl - wl_old) < 0.0001:
                break
            wl_old = wl  # copy(wl)
        # Compute variance
        sig2l = (linalg.norm(robs - Xn.dot(wl)) ** 2) / N
        if np.sqrt(np.square(sig2l - sig2l_old)) < 0.0001:
            break
        sig2l_old = sig2l  # copy(sig2l)
    wn = wl  # copy(wl)
    # sig2 = sig2l
    return wn, sig2l


# %% Francis Paper
# FormHistMatrix2
# Inputs:
# x = 2D-array varrying size dtype float64
# Whc = 1D-array 2, dtype float64
# LM = float64
# Outputs
# Xz = 3D-array varrying size dtype float 64
def FormHistMatrix2(x, Whc, Lm):
    [N, Ncells] = x.shape
    Np = N - Lm
    Mhc = len(Whc);
    Lhc = np.sum(Whc)
    X = np.zeros([int(Np), Mhc, Ncells])
    Lhself1 = np.zeros([Mhc, 1]);
    Lhself2 = copy(Lhself1)
    for j in range(0, Mhc):
        Lhself1[j] = np.sum(Whc[Mhc - j:Mhc])
        Lhself2[j] = np.sum(Whc[Mhc - j - 1:Mhc])

    for k in range(0, int(Np)):
        iStart = k + Lm - Lhc + 1
        iStop = k + Lm - Lhc
        for j in range(1, Mhc + 1):
            cInd = np.arange(iStart + Lhself1[j - 1], iStop + Lhself2[j - 1] + 1)
            cInd = cInd.astype(int)
            if len(cInd) == 1:
                X[k, Mhc - j, :] = x[cInd - 1, :]
            else:
                X[k, Mhc - j, :] = np.mean(x[cInd - 1, :], axis=0)

    Xz = copy(X)
    for cc in range(0, Ncells):
        ones_mat = np.ones([int(Np), 1])
        # avg_X = np.mean(X[:, :, cc], axis=0)
        avg_X = average(X[:, :, cc], 0).T

        dot_prod = np.dot(ones_mat, avg_X)
        Xz[:, :, cc] = X[:, :, cc] - dot_prod
    return Xz


@jit(["float64[:,:](float64[:,:], int64)", "float64[:,:](int64[:,:], int64)"], nopython=True, cache=True)
def average(x, axis):
    """
    Compute average of a 2D array along given dimension.
    Preserve dimensionality of the input array
    """
    in_shape = x.shape[::-1]
    result = np.zeros((in_shape[axis], 1))
    for ix in prange(in_shape[axis]):
        if axis == 0:
            r = x[:, ix].mean()
        else:
            r = x[ix, :].mean()
        result[ix] = r
    return result

# %% Francis Paper
# FormHistMatrix2
# Inputs:
# x = 2D-array varrying size dtype float64
# Whc = 1D-array 2, dtype float64
# LM = float64
# Outputs
# Xz = 3D-array varrying size dtype float 64
@jit(nopython=True)
def numba_form_hist_matrix2(x, Whc, Lm):
    [N, Ncells] = x.shape
    Np = N - Lm
    Mhc = len(Whc);
    Lhc = np.sum(Whc)
    X = np.zeros((int(Np), Mhc, Ncells))
    Lhself1 = np.zeros((Mhc, 1))
    Lhself2 = np.zeros((Mhc, 1))
    for j in prange(0, Mhc):
        Lhself1[j] = np.sum(Whc[Mhc - j:Mhc])
        Lhself2[j] = np.sum(Whc[Mhc - j - 1:Mhc])

    for k in range(0, int(Np)):
        iStart = k + Lm - Lhc + 1
        iStop = k + Lm - Lhc
        for j in range(1, Mhc + 1):
            start = int(iStart + Lhself1[j - 1, 0])
            stop = int(iStop + Lhself2[j - 1, 0] + 1)
            cInd = np.arange(start, stop)
            # cInd = cInd.astype(int)
            if len(cInd) == 1:
                X[k, Mhc - j, :] = x[cInd - 1, :]
            else:
                X[k, Mhc - j, :] = average(x[cInd - 1, :], 0)[:, 0]

    Xz = X.copy()
    for cc in range(0, Ncells):
        ones_mat = np.ones((int(Np), 1))
        avg_X = average(X[:, :, cc], 0)
        avg_X_t = avg_X.transpose()
        Xz[:, :, cc] = X[:, :, cc] - np.dot(ones_mat, avg_X_t)
    return Xz


def speed_comp(x, Whc, Lm):
    start = time.time()
    Xz = FormHistMatrix2(x, Whc, Lm)
    end = time.time()
    std_time = end - start
    print("Standard function = %s" % (std_time))

    start = time.time()
    Xz_jit = numba_form_hist_matrix2(x, Whc, Lm)
    end = time.time()
    jit_time = end - start
    print("JIT function = %s" % (jit_time))
    print(f'Speedup: {std_time/jit_time:.1f}')
    return np.allclose(Xz, Xz_jit)


if __name__ == '__main__':
    npzfile = np.load('StaticEstimCV_vars.npz')
    Xn = npzfile['Xn']
    robs = npzfile['robs']
    gamma = npzfile['gamma']
    LL = int(npzfile['LL'])
    LR = int(npzfile['LR'])
    npzfile = np.load('FormHistMatrix_args.npz')
    x = npzfile['x']
    Whc = npzfile['Whc']
    Lm = npzfile['Lm']
    numba_form_hist_matrix2(x, Whc, Lm)
    Xz_ok = speed_comp(x, Whc, Lm)
    print(Xz_ok)
