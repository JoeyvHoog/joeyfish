# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 15:41:32 2019

@author: DNP_Student_3
"""

#%% Load packages
import numpy as np
from numpy import linalg
import scipy 
import scipy.sparse
from scipy.stats import chi2
import scipy.io as sio
import time
from copy import copy
from numba import njit
from dask import delayed
from multiprocessing.pool import ThreadPool
import dask
import cProfile
import pstats
#%% Functions
#@guvectorize([(float64[:], float64[:])],'(n,n)->(n,n)')
@njit
def MeanCol(x):
    res= np.zeros((1,len(x[0])))
    for i in range(0,len(x[0])):
        res[:,i] = np.mean(x[:,i])
    return res
# FormHistMatrix2
#Inputs:
# x = 2D-array varrying size dtype float64
# Whc = 1D-array 2, dtype float64
# LM = float64
# Outputs
# Xz = 3D-array varrying size dtype float 64
@njit
def FormHistMatrix2(x,Whc,Lm):
    [N,Ncells] = x.shape
    Np = N -Lm
    Mhc = len(Whc); Lhc = np.sum(Whc)
    X = np.zeros((int(Np),Mhc,Ncells))
    Lhself1 = np.zeros((Mhc,1)); Lhself2 = Lhself1.copy()
    for j in range(0,Mhc):
        Lhself1[j] = np.sum(Whc[Mhc-j:Mhc])
        Lhself2[j] = np.sum(Whc[Mhc-j-1:Mhc])
    
    for k in range(0,int(Np)):
        iStart = k + Lm - Lhc + 1
        iStop = k + Lm - Lhc 
        for j in range(1,Mhc+1):
            cind = np.arange(iStart+Lhself1[j-1][0], (iStop + Lhself2[j-1] + 1)[0])
            cInd = cind.astype(np.int32)
            if len(cInd) == 1:
                X[k,Mhc-j,:] = x[cInd-1,:]
            else:
                X[k,Mhc-j,:] = MeanCol(x[cInd-1,:])
                
    Xz = X.copy()
    for cc in range(0,Ncells):
        Xz[:,:,cc] = X[:,:,cc] - np.dot(np.ones((int(Np),1)),MeanCol(X[:,:,cc]))
    return Xz

## FormFullDesign2
def FormFullDesign2(Xcross,Xself,ct):
    [Np,Mhc,Ncells,R] = np.shape(Xcross)
    cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
    ## Full Model
#    Xfc = np.squeeze(Xself,axis=2)
    Xfc = copy(Xself)
    for cc in range(0,Ncells-1):
        Xfc = np.concatenate((np.squeeze(Xfc),np.squeeze(Xcross[:,:,cellC[cc],:])),axis=1)
    Xf = copy(Xfc)
    ## Form effective covariate matrix
    Mf = np.shape(Xf)[1]
    Xeff = np.zeros([R*Np,Mf])
    if Xf.ndim ==3:
        for r in range(1,R+1):
            Xeff[(r-1)*Np:r*Np,:] = Xf[:,:,r-1]
    else:
        for r in range(1,R+1):
            Xeff[(r-1)*Np:r*Np,:] = Xf[:,:]
    Dnf = np.diag(np.sqrt(np.var(Xeff,axis=0)))
    Xneff = linalg.solve(Dnf.T,Xeff.T).T
    return Xneff, Dnf

## StaticEstimCV
# Inputs:
# Xn = 2D-array shape 1997,20 dtype float64
# robs = 2D-array shape 1997,1 dtype float64
# gamma = float64
# LL = int
# LR = int
# Outputs:
# wn = 2D-array shape 20,1 dtype float64
# sig2 = float64
@njit
def StaticEstimCV(Xn,robs,gamma,LL,LR):
    [N,M] = Xn.shape
    # Estimate variance using maximum-likelihood
    XXn = Xn.T.dot(Xn)
    Xdn = Xn.T.dot(robs)
    wML = linalg.solve(XXn,Xdn)
    sig2ML = (linalg.norm(robs-np.dot(Xn,wML))**2)/N
    # Set initials based on ML estimate
    sig2l = np.expand_dims(np.array((sig2ML)),0)
    wl = wML.copy()
    sig2l_old = np.expand_dims(np.array((10000.)),0)
    for ll in range(0,LR):
        XXl = XXn/np.sqrt(sig2l*N*np.log(M))
        Xdl = Xdn/np.sqrt(sig2l*N*np.log(M))
        # Step-size selection
        rhom = np.max(np.absolute(linalg.eigvals(XXl)))## Still needs checking 
        al = 0.9/rhom; alg  = al*gamma 
        wl_old = np.ones_like(wl)*np.inf
        for l in range(0,LL):
#            gl = Xdl - np.dot(XXl,wl)
#            x = wl + np.dot(al,gl)
#            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-np.dot(gamma,al),0))
            x = wl + al*(Xdl-XXl.dot(wl))
            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-alg,0))
            if linalg.norm(wl-wl_old) < 0.0001:
                break
            wl_old = wl.copy()
        #Compute variance
        sig2l = np.expand_dims(np.array(((linalg.norm(robs-np.dot(Xn,wl))**2)/N)),0)
        if linalg.norm(sig2l - sig2l_old) < 0.0001:
            break
        sig2l_old = sig2l.copy()            
    wn = wl.copy(); sig2 = sig2l.copy()
    return  wn, sig2

## StaticEstim
@njit
def StaticEstim(Xn,robs,gamma,LL,LR):
    [N,M] = Xn.shape
    # Estimate variance using maximum-likelihood
    XXn = Xn.T.dot(Xn)
    Xdn = Xn.T.dot(robs)
    wML = linalg.solve(XXn,Xdn)
    sig2ML = (linalg.norm(robs-np.dot(Xn,wML))**2)/N
    # Set initials based on ML estimate
    sig2l = np.expand_dims(np.array((sig2ML)),0)
    wl = wML.copy()
    sig2l_old = np.expand_dims(np.array((10000.)),0)
    for ll in range(0,LR):
        XXl = XXn/np.sqrt(sig2l*N*np.log(M))
        Xdl = Xdn/np.sqrt(sig2l*N*np.log(M))
        # Step-size selection
        rhom = np.max(np.absolute(linalg.eigvals(XXl)))## Still needs checking 
        al = 0.9/rhom; alg  = al*gamma 
        wl_old = np.ones_like(wl)*np.inf
        for l in range(0,LL):
#            gl = Xdl - np.dot(XXl,wl)
#            x = wl + np.dot(al,gl)
#            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-np.dot(gamma,al),0))
            XXlwl = XXl.dot(wl)
            x = wl + al*(Xdl-XXlwl)
            wl = np.multiply(np.sign(x),np.maximum(np.absolute(x)-alg,0))
            if linalg.norm(wl-wl_old) < 0.0001:
                break
            wl_old = wl.copy()
        #Compute variance
        sig2l = np.expand_dims(np.array(((linalg.norm(robs-np.dot(Xn,wl))**2)/N)),0)
        if linalg.norm(sig2l - sig2l_old) < 0.0001:
            break
        sig2l_old = sig2l.copy()            
    wn = wl.copy(); sig2 = sig2l.copy()
    # Compute gradient at updated wl and sig2l
    gn = (Xdn - np.dot(XXn,wn))/sig2
    Hn = XXn/sig2
    # Compute Unbiased Deviance
    B = np.dot(gn.T,linalg.solve(Hn,gn)) ## Needs checking
    Dev = -N*np.log(sig2) + B
    return wl,sig2,Dev,B
    
## CrossValidModTrial
def CrossValidModTrial(Resp,gammacv,Whcv,Whscv,LLcv,LRcv):
    [N,R,Ncells] = np.shape(Resp)
    Jcost = np.zeros([Ncells,len(gammacv),len(Whcv)])
    
    for ii in range(0,len(Whcv)):
        Whi = Whcv[ii]; Mhc = len(Whi); Lhc = np.sum(Whi)
        #Prepare Self-History Conditions
        Whsi = Whscv[ii]; Mhcs = len(Whsi); Lhcs = np.sum (Whsi)
        Lm = np.max([Lhc,Lhcs])
        Np = N - Lm
        # Form Cross-History Covariates Matrices
        Xcz = np.zeros([int(Np),Mhc,Ncells,R])
        for r in range(0,R):
            Xcz[:,:,:,r] = FormHistMatrix2(Resp[:,r,:],Whi,Lm)
        # Form Observation arrays for all cells
        Robs = copy(Resp[int(Lm)::,:,:])
        # Zero mean observation vectors
        for cc in range(0,Ncells):
            Robs[:,:,cc] = Robs[:,:,cc] - np.dot(np.ones([int(Np),1]),np.expand_dims(np.mean(Robs[:,:,cc],axis=0),0))
        for ct in range(0,Ncells):
            N_indto = str(ct+1)
            print('Cross-validation on cell ' + N_indto + ' ...')
            cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
            # Form Self-History Covariates
            Xcsz = np.zeros([int(Np),Mhcs,R])
            for r in range(0,R):
                Xcsz[:,:,r] = np.squeeze(FormHistMatrix2(np.expand_dims(Resp[:,r,ct],1),Whsi,Lm),axis=2)
            # Form Effective Response Vector
            robsz = Robs[:,:,ct]
            # Form Full model design matrix
            Xfc = copy(Xcsz)
            for cc in range(0,Ncells-1):
                Xfc = np.concatenate((Xfc,Xcz[:,:,cellC[cc],:]),axis=1)
            Xf = copy(Xfc)
            # Prepare for Cross-Validation
            sig2test = np.zeros([R,len(gammacv)])
            # Training stage on all but one repititions
            for r in range(0,R):
                rc = np.arange(0,R); rc = np.delete(rc,r)
                print('CV @ repetition # ' + str(r+1) + ' ...')
                #Form Testing Data
                Xtest = Xf[:,:,r]
                rtest = robsz[:,r]; rntest = rtest/np.sqrt(np.var(rtest))
                ## Form Training Data
                [Xntrain,Dntrain] = FormFullDesign2(Xcz[:,:,:,rc],Xcsz[:,:,rc],ct)
                rtrain = robsz[:,rc]
                rntrain = rtrain/np.sqrt(np.var(rtrain,axis=0))
                SigTests = np.zeros([1,len(gammacv)])
                for jj in range(0,len(gammacv)):
                    gammaj = gammacv[jj]
                    [wntrain,non] = StaticEstimCV(Xntrain,rntrain,gammaj,LLcv,LRcv)
                    wtrain = linalg.solve(Dntrain,wntrain)
                    ## Test stage: Compute variance for give repetition
                    SigTests[:,jj] = (linalg.norm(np.expand_dims(rntest,1)-np.dot(Xtest,wtrain))**2)/Np
                sig2test[r,:] = copy(SigTests)
            Jcost[ct,:,ii] = np.mean(sig2test,axis=0)
    Jcost = np.squeeze(Jcost,axis=2)
    gammaOpt = np.zeros([Ncells,1])
    for ct in range(0,Ncells):
        LLct = Jcost[ct,:]
        indgamma = np.where(LLct == np.min(np.min(LLct)))[0]
        if len(gammacv) > 1:
            indgamma = np.max(indgamma)
        gammaOpt[ct] = gammacv[int(indgamma)]
    return gammaOpt, Jcost

## LF_estimGC
def LF_EstimGC(cellC,Mhc,Mhcs,gamma,LL,LR,Xneff,reff,Np,Ntrials,Devf):
    Br = np.zeros(np.shape(cellC))
    Devd = np.zeros([len(cellC)])
    rhatr_tmp = {}
    sig2rtotal_tmp = np.zeros([len(cellC)])
    for iC in range(0,len(cellC)):
        cf = cellC[iC]
        # Reduced Model
        Xnefr = copy(Xneff)
        cc = np.where(cellC == cf)[0]
        Cdel = np.arange(cc*Mhc+Mhc,(cc+1)*Mhc+Mhcs)
        Xnefr = np.delete(Xnefr,Cdel,1)
        #Estimation Setting
        [wnr,sig2r,Devr,Br[iC]] = StaticEstim(Xnefr,reff,gamma,LL,LR)
        #Save estimated parameters
        sig2rtotal_tmp[iC] = copy(sig2r)
        # Reconstructed observation vector reduced model
        rhatr = np.zeros([int(Np),Ntrials])
        for r in range(1,Ntrials+1):
            Xnr = copy(Xnefr[int((r-1)*Np):int(r*Np),:])
            rhatr[:,r-1] = np.dot(Xnr,wnr)
        rhatr_tmp[iC] = copy(rhatr)
        #Compute difference of Deviance Staistic
        Devd[iC] = Devf - Devr
    return Devd,rhatr_tmp,sig2rtotal_tmp,Br
        
## FDRcontrolBH
def FDRcontrolBH(Devtotal,DkSmth,alpha,Md,wftotal,Whc,Mlow):
    [Ncells,N] = np.shape(Devtotal)
    Mhc = len(Whc)
    [Mf] = np.shape(wftotal[0]); Mhcs = Mf - (Ncells-1)*Mhc
    Nl = Ncells*(Ncells-1) # Number of links
    pth = np.zeros([1,Nl])
    for i in range(1,Nl+1):
        pth[:,i-1]=alpha*(i)/(Nl*np.log(Nl))
    pth = np.squeeze(pth,axis=0)
    alpham = alpha*(Nl+1)/(2*Nl*np.log(Nl)) # Mean FDR by BH rule
    IGC = np.zeros(np.shape(Devtotal)); IGClow = copy(IGC); IGChigh = copy(IGC)
    # Compute p-values
    Pv = chi2.sf(Devtotal,Md)
    for t in range(0,N):
        Pvt = copy(Pv)
        Pvtsort= np.sort(Pvt,axis=None)
        Indsrt = np.argsort(Pvt.T,axis=None)
        Pvtsortx = Pvtsort[0:Nl]
        cnt = 0
        while Pvtsortx[cnt] < pth[cnt]:
            cnt=cnt+1
            if cnt == Nl:
                break
        siglind = Indsrt[0:cnt]
        cols = np.ceil(np.divide(siglind+1,Ncells))
        rows = (siglind+1) - np.dot((cols-1),Ncells)
        for ee in range(0,len(siglind)):
            IGC[int(rows[ee]-1),int(cols[ee]-1)] = 1 - alpham - scipy.stats.ncx2.cdf(scipy.stats.distributions.chi2.ppf((1-alpham),Md),Md,DkSmth[int(rows[ee]-1),int(cols[ee]-1)])
            whati = wftotal[rows[ee]-1]
            CellC = np.arange(0,Ncells); CellC = np.delete(CellC,int(rows[ee]-1))
            jji = np.where(CellC == cols[ee]-1)[0]
           
            wseff = np.multiply(whati[int(jji*Mhc+Mhc):int(jji*Mhc+Mhcs+2)],Whc.T)
            wslow = copy(wseff[0:Mlow])
            wshigh = copy(wseff[Mlow::])
            if np.sign(np.sum(wslow)) != 0:
                IGClow[int(rows[ee]-1),int(cols[ee]-1)] = np.sign(np.sum(wslow))*IGC[int(rows[ee]-1),int(cols[ee]-1)]
            else:
                IGClow[int(rows[ee]-1),int(cols[ee]-1)] = IGC[int(rows[ee]-1),int(cols[ee]-1)]
            if np.sign(np.sum(wshigh)) !=0:
                IGChigh[int(rows[ee]-1),int(cols[ee]-1)] = np.sign(np.sum(wshigh))*IGC[int(rows[ee]-1),int(cols[ee]-1)]
            else:
                IGChigh[int(rows[ee]-1),int(cols[ee]-1)] = IGC[int(rows[ee]-1),int(cols[ee]-1)]
                
    return IGClow, IGChigh


#computeGrangerCausality
def computeGrangerCausality(DFF,Ncells, cellids, gammacv,alpha, Md, WH, Mhc, Mhcs, LL, LR,LLcv, LRcv, Mlow):
    Devtotal = np.zeros([Ncells,Ncells])
    DevtotalC = {}
    DLLtotal = copy(Devtotal)
    wftotal = {}
    robstotal = {}
    sig2ftotal = np.zeros([Ncells,1])
    sig2rtotal = np.zeros([Ncells,Ncells])
    sig2rtotalC = {}
    Bftotal = np.zeros([Ncells,1])
    Brtotal = np.zeros([Ncells,Ncells])
    rhatftotal = {}
    rhatrtotal = {}
    rhatrtotalC = {}
    gammaOpttotal = np.zeros([Ncells,1])
    BrtotalC = {}
    
    #Select Cells
    DFF = DFF[:,:,cellids-1]
    [Nframes,Ntrials,Ncells] = np.shape(DFF)
    # Cross-History Kernel
    Whc = np.array([1,WH*np.ones(Mhc-1)])
    Lhc = np.sum(Whc)
    Whcv = {}; Whcv[0] = Whc
    #Self-History Kernel
    Whcs = np.array([1,WH*np.ones(Mhc-1)])
    Whscv= {}; Whscv[0] = Whcs
    Lhcs = np.sum(Whcs)
    Mf = (Ncells-1)*Mhc+Mhcs
    Mr = (Ncells-2)*Mhc+Mhcs
    
    # Cross-Validation
    [gammaOpt,Jcost] = CrossValidModTrial(DFF,gammacv,Whcv,Whscv,LLcv,LRcv)
    gammaOpttotal[:,0] = np.squeeze(gammaOpt,axis=1)
    
    # Form Cross-History Covariate Arrays
    Lm = np.max([Lhc,Lhcs])
    Np = Nframes - Lm
    Xcz = np.zeros([int(Np),Mhc,Ncells,Ntrials])
    for r in range(0,Ntrials):
        Xcz[:,:,:,r] = FormHistMatrix2(DFF[:,r,:],Whc,Lm)
    # Form Observation arrays for all cells
    Robs = copy(DFF[int(Lm)::,:,:])
    # Zero-mean the observation vectors
    for cc in range(0,Ncells):
        Robs[:,:,cc] = Robs[:,:,cc] - np.dot(np.ones([int(Np),1]),np.expand_dims(np.mean(Robs[:,:,cc],axis=0),0))
    for ct in range(0,Ncells):
        print('Estimating G-Causality to cell ' + str(ct+1) + ' ...')
        cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
        gamma = gammaOpt[ct]
        
        # Form Self-history Covariate matrix
        Xcsz = np.zeros([int(Np),Mhc,Ntrials])
        for r in range(0,Ntrials):
            Xcsz[:,:,r] = np.squeeze(FormHistMatrix2(np.expand_dims(DFF[:,r,ct],1),Whcs,Lm),axis=2)
        # Form Effective Response Vector
        robsz = Robs[:,:,ct]
        reff = robsz.flatten('F'); Neff = len(reff)
        
        ## Full Model
        # Form Standardize Full Model Covariate Matrix
        [Xneff,Dnf] = FormFullDesign2(Xcz,Xcsz,ct)
        # Filtering/Estimation Setting
        [wnf,sig2f,Devf,Bf] = StaticEstim(Xneff,reff,gamma,LL,LR) 
        # Save Tuning AR Coeffs full model for each cell
        wftotal[ct] = copy(wnf)
        sig2ftotal[ct] = copy(sig2f)
        robstotal[ct] = copy(robsz)
        #Reconstructed observation vector full model
        rhatf = np.zeros([int(Np),Ntrials])
        for r in range(1,Ntrials+1):
            Xnf = Xneff[int((r-1)*Np):int(r*Np),:]
            rhatf[:,r-1] = np.dot(Xnf,wnf)
        rhatftotal[ct] = copy(rhatf)
        Bftotal[ct] = copy(Bf)
        [Devd,rhatr_tmp,sig2rtotal_tmp,Br] = LF_EstimGC(cellC,Mhc,Mhcs,gamma,LL,LR,Xneff,reff,Np,Ntrials,Devf)
        sig2rtotalC[ct] = copy(sig2rtotal_tmp)
        BrtotalC[ct] = copy(Br)
        DevtotalC[ct] = copy(Devd)
        rhatrtotalC[ct] = copy(rhatr_tmp)
        # Insert results into output variables
    for ct in range(0,Ncells):
        cellC = np.arange(0,Ncells); cellC = np.delete(cellC,ct)
        sig2rtotal[ct,cellC] = sig2rtotalC[ct]
        Brtotal[ct,cellC] = BrtotalC[ct]
        Devtotal[ct,cellC] = DevtotalC[ct]
        for iC in range(0,len(cellC)):
            if ct == cellC[iC]:
                pass
            else:
                rhatrtotal[ct,cellC[iC]] = rhatrtotalC[ct]
    # Statistical Test: J-statistics
    DkSmth = Devtotal - Md
    [GCJL,GCJH] = FDRcontrolBH(Devtotal,DkSmth,alpha,Md,wftotal,Whc,Mlow)
    return Devtotal, GCJL, GCJH, wftotal, sig2ftotal, sig2rtotal, robstotal, rhatftotal, rhatrtotal, Bftotal, Brtotal, cellids, gammaOpttotal, Whc, Whcv, Whcs, gammaOpt, Jcost
     
## Main Function   
def GrangerCausality(DFF,MaxCells):
#Parameter Estimation Settings
    LL = 1000
    LR = 10
    LLcv = 100
    LRcv = copy(LR)
    # Cross-history covariance settings
    WH = 2 # Window Length
    Mhc = 2 # Samples per cell
    # Self history covariance settings
    Mhcs = 2 # Samples per cell
    # Check for diff gamma for each cell
    gammacv = np.arange(0,3.2,0.2)
    # Satistical Test: J-statistics based on FDR
    Md = copy(Mhc)
    alpha = 0.5 # sig. level
    # Determine which segments of analysis window are used to determine inhibitory versus excitatory links
    Mlow = 2 # Limit length of window
    
# Gather data
    #Select subset with highest response var
    varR = np.zeros([np.size(DFF,2),1])
    if MaxCells < np.size(DFF,2):
        RR = np.size(DFF,1)
        for c in range(0,np.size(DFF,2)):
            varR[c] = np.mean(np.var(DFF[:,:,c]))
        varS = np.divide(np.dot(varR,RR),np.sum(RR))
        indm = np.argsort(varS[::-1])
        cellids = np.sort(indm[0:MaxCells])
    else:
        cellids = np.arange(1,MaxCells+1)
    Ncells = np.minimum(np.size(DFF,2),MaxCells)
# GCAnalysis
    with dask.config.set(pool=ThreadPool(4)):          
        r = delayed(computeGrangerCausality)(DFF, Ncells, cellids, gammacv,alpha, Md, WH, Mhc, Mhcs,
                   LL, LR,LLcv, LRcv, Mlow)
        comp_output = r.compute(num_workes=4)
        Devtotal,GCJL,GCJH,wftotal,sig2ftotal,sig2rtotal,robstotal,rhatftotal,\
        rhatrtotal,Bftotal,Brtotal,cellids,gammaOpttotal,Whc,Whcv,Whcs,gammaOpt,Jcost = comp_output
#    [Devtotal,GCJL,GCJH,wftotal,sig2ftotal,sig2rtotal,robstotal,rhatftotal,
#    rhatrtotal,Bftotal,Brtotal,cellids,gammaOpttotal,Whc,Whcv,Whcs,gammaOpt,Jcost]  = computeGrangerCausality(DFF,
#    Ncells, cellids, gammacv,alpha, Md, WH, Mhc, Mhcs,
#    LL, LR,LLcv, LRcv, Mlow)

    Output = {'Parameters':{'MaxCells': MaxCells,'Whc':Whc,'Whcs':Whcs,'LL':LL,'LR':LR,'WH':WH,'Mhc':Mhc,'Mhcs':Mhcs,'gammacv':gammacv,'Md':Md,'alpha':alpha,'Mlow':Mlow},
              'Cross-validation':{'gammaOpt':gammaOpt,'Jcost':Jcost,'Whcv':Whcv,'LLcv':LLcv,'LRcv':LRcv},
              'GCAnalysis':{'Devtotal':Devtotal,'GCJL':GCJL,'GCJH':GCJH,'wftotal':wftotal,'sig2ftotal':sig2ftotal,'sig2rtotal':sig2rtotal,'robstotal':robstotal,'rhatftotal':rhatftotal,'rhatrtotal':rhatrtotal,'Bftotal':Bftotal,'Brtotal':Brtotal,'cellids':cellids,'gammaOpttotal':gammaOpttotal}}
    return Output    
   
if __name__ == '__main__':    
#%%
    TestData = sio.loadmat('TestData50')
    Data  = TestData['ans']
#%% Analysis
    DFF = np.copy(Data); MaxCells = 50
#    start = time.time()
    cProfile.run('GrangerCausality(DFF,MaxCells)', 'GrangerCausality.profile')
#    Output = GrangerCausality(DFF,MaxCells)
#    end = time.time()
#    tot_time = end - start
#    print(f'Total time: {tot_time:.2f} s')
    stats = pstats.Stats('GrangerCausality.profile')
    stats.strip_dirs().sort_stats('time').print_stats()


































    
    
    
    